<?php

    require_once('model.php');
    session_start();
    //redirect user to login.php if he tries to pass the login page
    if(!isset($_SESSION['user'])){
        header("Location:login.php");
   } else{
        $user = $_SESSION['user'];
    }
?>


<html>
    <head>
    
    </head>
    <body>

        <p>
            You are Logged In as <?php print $user->get_username() ?>
            .....This is the main page.
            <a href = "index.php?op=logout">Logout</a>
            <form method="POST" action="index.php">
            <input type="submit" name = "op" value="logout"/>
            </form>
        </p>

    </body>
</html>