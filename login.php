<?php
//    require_once('login.php');
    session_start();
    //redirect user to main.php if he logs in
    if(isset($_SESSION['user'])){
        header("Location:main.php");
    }
?>

<html>
    <head>
        <style>
            #login-controls {
                margin: 0 auto;
                border: 1px solid #ccc;
                padding: 50px;
                width: 300px;
            }

            .error-text{
                color: #f00;
            }

        </style>
    </head>
    <body>
    
            <div id="login-controls">
                <h2> Login</h2>
                <!-- shows error message -->
                <?php if(isset($_GET['err'])){?>
                    <div class="error-text"><p> Login Incorrect. Please Try Again </p></div>
                <?php } ?> 


                <form method="POST" action="index.php">
                    <p>Username: <br>
                        <input type="text" name = "user"/>
                    </p>
                    <p>Password: <br>
                        <input type="password" name = "pass"/>
                    </p> 
                    <input type="submit" name = "op" value="login"/>                   
                
                </form>
            
            
            </div>
    
    </body>
</html>